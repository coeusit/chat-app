class Session
    def initialize(session_key:)
      if session_key.present? && read_cache(session_key).present?
        @session_key = session_key
      else
        create_session
      end
    end
  
    def authenticate(email, password)
      u = User.find_by(email: email)
      result = u&.authenticate(password) || false
      if result
       set_user(u)
      end
      result
    end
  
    def set_user(u)
      session_data = read_cache
      session_data['user_id'] = u.id
      write_cache(session_data)
    end
  
    def key
      @session_key
    end
  
    def secret
      cached = read_cache
      cached.nil? ? nil : cached['session_secret']
    end
  
    def current_user
      uid = user_id
      uid.nil? ? nil : User.find(uid)
    end
  
    def user_id
      cached = read_cache
      cached.nil? ? nil : cached['user_id']
    end
  
    def deauth
      session_data = read_cache
      session_data['user_id'] = nil
      write_cache(session_data)
    end
  
    private
  
    def write_cache(data)
      Rails.cache.write("sessions/#{@session_key}", data.to_json, expires_in: 1.day)
    end
  
    def read_cache(session_key=nil)
      raw_data = Rails.cache.read("sessions/#{(session_key.nil? ? @session_key : session_key)}")
      if raw_data.nil?
        return nil
      else
        return JSON.parse(raw_data)
      end
    end
  
    def create_session
      session = {
        user_id: nil,
        session_key: SecureRandom.uuid,
      }
      @session_key = session[:session_key]
      write_cache(session)
    end
  end