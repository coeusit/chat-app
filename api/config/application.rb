require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module Api
  class Application < Rails::Application
    config.load_defaults 7.0
    config.autoload_paths << "#{Rails.root}/lib"
    config.action_cable.allowed_request_origins = ['https://chatapp.coeus-it.com','http://localhost:9000']
    config.api_only = true
  end
end
