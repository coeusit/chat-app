class GraphqlController < ApplicationController
  def execute
    variables = prepare_variables(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    s = Session.new(session_key: raw_token)
    context = {
      current_user: s.current_user,
      session: s,
      raw_token: raw_token,
      current_application_context: (s.current_user.nil? ? "SESSION/#{s.key}" : "USER/#{s.current_user.id}")
    }
    result = ApiSchema.execute(query, variables: variables, context: context, operation_name: operation_name)

    if result.subscription?
      response.headers['X-Subscription-Channel'] = result.context[:subscription_id]
      result[:data] ||= {}
    end
    
    render json: result
  rescue StandardError => e
    raise e unless Rails.env.development?
    handle_error_in_development(e)
  end

  private

  def raw_token
    token = request.headers['Authorization']&.split&.last
    return token == 'null' ? nil : token
  end

  def prepare_variables(variables_param)
    case variables_param
    when String
      if variables_param.present?
        JSON.parse(variables_param) || {}
      else
        {}
      end
    when Hash
      variables_param
    when ActionController::Parameters
      variables_param.to_unsafe_hash
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{variables_param}"
    end
  end

  def handle_error_in_development(e)
    logger.error e.message
    logger.error e.backtrace.join("\n")

    render json: { errors: [{ message: e.message, backtrace: e.backtrace }], data: {} }, status: 500
  end
end
