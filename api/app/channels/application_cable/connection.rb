module ApplicationCable
  class Connection < ActionCable::Connection::Base
    def connect
      self.session = find_session
    end

    def current_application_context
      session.current_user.nil? ?  "SESSION/#{session.key}" : "USER/#{session.user_id}"
    end

    def current_user_id
      session.user_id
    end

    def current_user
      session.current_user
    end

    def session
      @session ||= find_session
    end

    protected

    attr_writer :session

    def find_session
      Session.new(session_key: raw_token)
    end

    def raw_token
      token = request.query_parameters['token']
      return token == 'null' ? nil : token
    end
  end
end
