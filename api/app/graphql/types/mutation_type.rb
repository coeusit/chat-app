module Types
  class MutationType < Types::BaseObject
    field :message_create, mutation: Mutations::MessageCreate
    field :logout, mutation: Mutations::Logout
    field :login, mutation: Mutations::Login
  end
end
