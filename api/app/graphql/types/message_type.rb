# frozen_string_literal: true

module Types
  class MessageType < Types::BaseObject
    field :uuid, String
    field :user, Types::UserType, null: false
    field :chatroom, Types::ChatroomType, null: false
    field :content, String
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
