module Types
  class SubscriptionType < Types::BaseObject
    field :chatroom_messages, subscription: Subscriptions::ChatroomMessages
  end
end