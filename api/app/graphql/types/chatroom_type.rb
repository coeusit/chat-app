# frozen_string_literal: true

module Types
  class ChatroomType < Types::BaseObject
    field :uuid, String
    field :name, String
    field :messages, [Types::MessageType]
  end
end
