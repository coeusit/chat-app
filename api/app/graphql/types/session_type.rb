# frozen_string_literal: true

module Types
    class SessionType < Types::BaseObject
        field :uuid, String
        field :user, Types::UserType, null: true
    end
end