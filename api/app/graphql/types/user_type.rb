# frozen_string_literal: true

module Types
  class UserType < Types::BaseObject
    field :uuid, String
    field :name, String
    field :messages, [Types::MessageType]
  end
end
