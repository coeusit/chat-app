module Subscriptions
    class ChatroomMessages < BaseSubscription
        field :messages, [Types::MessageType], null: true
        argument :chatroom_uuid, String

        def subscribe(chatroom_uuid:)
            chatroom = Chatroom.find_by(uuid: chatroom_uuid)
            {
                messages: chatroom.messages.all
            }
        end
    end
end