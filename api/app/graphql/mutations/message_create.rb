module Mutations
  class MessageCreate < BaseMutation
    description "Creates a new message"

    field :message, Types::MessageType, null: false

    argument :chatroom_uuid, String, required: true
    argument :message_input, Types::MessageInputType, required: true

    def resolve(chatroom_uuid:, message_input:)
      chatroom = Chatroom.find_by(uuid: chatroom_uuid)

      unless chatroom
        raise GraphQL::ExecutionError.new "Chatroom not found with UUID #{chatroom_uuid}"
      end

      message = context[:current_user].messages.build(**message_input.to_h.merge(chatroom_id: chatroom.id))

      unless message.save
        raise GraphQL::ExecutionError.new "Error creating message", extensions: message.errors.to_hash
      end

      { message: message }
    end
  end
end
