module Mutations
  class Login < BaseMutation
    field :user, Types::UserType, null: true

    argument :email, String, required: true
    argument :password, String, required: true

    def resolve(email:, password:)
      user = User.find_by(email: email)
      if user && context[:session].authenticate(email, password)
        { user: user }
      else
        { user: nil }
      end
    end
  end
end
