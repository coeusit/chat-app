module Queries
    class Chatrooms < GraphQL::Schema::Resolver
        description "Chatroom listing"

        type [Types::ChatroomType], null: false

        def resolve
            Chatroom.all
        end
    end
end