module Queries
    class CurrentSession < GraphQL::Schema::Resolver
        description "Token generation"

        type Types::SessionType, null: false

        def resolve
            {
                uuid: context[:session].key,
                user: context[:current_user]
            }
        end
    end
end