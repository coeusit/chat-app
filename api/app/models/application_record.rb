class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
  before_create :set_uuid

  private

  def set_uuid
    return if self.class.column_names.exclude?("uuid") || uuid.present?
    self.uuid = SecureRandom.uuid
  end
end
