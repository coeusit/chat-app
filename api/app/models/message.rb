class Message < ApplicationRecord
  belongs_to :user
  belongs_to :chatroom
  after_create :push_to_users
  def push_to_users
    ApiSchema.subscriptions.trigger(
      :chatroom_messages,
      {
        chatroom_uuid: self.chatroom.uuid
      },
      {
        messages: [self]
      }
    )
  end
end
