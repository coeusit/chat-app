require "faker"

if ENV['RAILS_ENV'] == "development"
    if User.all.empty?
        20.times do |index|
            User.create(
                email: "demo#{index}@example.com",
                name: Faker::Internet.unique.username,
                password: "password",

            )
        end
    end
    if Chatroom.all.empty?
        10.times do
            chatroom = Chatroom.create(
                name: Faker::Hobby.unique.activity
            )
            30.times do
                chatroom.messages.create(
                    user: User.all.order("RAND()").first,
                    content: Faker::Lorem.paragraph
                )
            end
        end
    end
end