Sample chat application using a Vue 3 client with a Rails server, using GraphQL, Actioncable/GraphQL Subscriptions, Redis session management and Docker-compose.

To install the Docker-compose stack, execute this from the project root:
```
docker-compose -f docker-compose.development.yml up -d
```

To prepare the database, run this from the shell of the **api** container
```
rake db:migrate
rake db:seed
```

To run the client application in Quasar's development mode, execute this from the client directory:
```
GRAPHQL_URI=http://localhost:3015/graphql GRAPHQL_URI_WS=ws://localhost:3015/cable quasar dev
```