import { gql } from 'graphql-tag';

const ChatroomMessagesSubscription = gql`
  subscription ($chatroomUuid: String!) {
    chatroomMessages(chatroomUuid: $chatroomUuid) {
      messages {
        uuid
        content
        user {
          uuid
          name
        }
      }
    }
  }
`;

export default ChatroomMessagesSubscription;
