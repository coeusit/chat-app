import { gql } from 'graphql-tag';

const ChatroomsQuery = gql`
  query {
    chatrooms {
      uuid
      name
    }
  }
`;

export default ChatroomsQuery;
