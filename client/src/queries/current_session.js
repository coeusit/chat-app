import { gql } from 'graphql-tag';

const CurrentSessionQuery = gql`
  query {
    currentSession {
      uuid
      user {
        uuid
        name
      }
    }
  }
`;

export default CurrentSessionQuery;
