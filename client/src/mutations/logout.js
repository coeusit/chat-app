import gql from 'graphql-tag'

const LogoutMutation = gql`
  mutation {
    logout(input: {}) {
      success
    }
  }
`
export default LogoutMutation
