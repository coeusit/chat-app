import gql from 'graphql-tag'

const LoginMutation = gql`
  mutation ($email: String!, $password: String!) {
    login(input: { email: $email, password: $password }) {
      user {
        uuid
        name
      }
    }
  }
`
export default LoginMutation
