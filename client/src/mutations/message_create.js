import gql from 'graphql-tag'

const MessageCreateMutation = gql`
  mutation ($chatroomUuid: String!, $messageInput: MessageInput!) {
    messageCreate(input: { chatroomUuid: $chatroomUuid, messageInput: $messageInput }) {
      message {
        uuid
      }
    }
  }
`
export default MessageCreateMutation
