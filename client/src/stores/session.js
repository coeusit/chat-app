import { defineStore } from 'pinia'

export const useSessionStore = defineStore('session', {
  state: () => ({
    initialized: false,
    authenticated: false,
    name: '',
    uuid: null
  }),

  getters: {
  },

  actions: {
    setUser(user) {
      this.authenticated = user !== null
      this.name = user?.name || null
      this.uuid = user?.uuid || null
    },
    setInitialized() {
      this.initialized = true
    }
  }
})
