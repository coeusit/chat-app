import { ApolloClient /*, createHttpLink */ } from '@apollo/client/core'
import { ApolloClients } from '@vue/apollo-composable'
import { boot } from 'quasar/wrappers'
import { getClientOptions } from 'src/apollo'
import { watchEffect } from 'vue'

let apolloClient = null

export default boot(
  /* async */ ({ app }) => {
    watchEffect(() => {
      // This will be re-run whenever the session token changes.
      const options = /* await */ getClientOptions(/* {app, router ...} */)
      apolloClient = new ApolloClient(options)

      const apolloClients = {
        default: apolloClient,
      }
      app.provide(ApolloClients, apolloClients)
    })
  }
)

export { apolloClient }
